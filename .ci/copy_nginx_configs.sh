#!/usr/bin/env ash
mkdir $TEST_DIR
cp -rv $DIRECTORY/docker/nginx/h5bp $TEST_DIR/
cp -rv $DIRECTORY/docker/nginx/conf.d $TEST_DIR/
cp -rv $DIRECTORY/docker/nginx/include $TEST_DIR/
cp -v $DIRECTORY/docker/nginx/nginx.conf $TEST_DIR/

