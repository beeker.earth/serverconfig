#!/bin/ash

mkdir -p /etc/letsencrypt/live/nozo.tv/
cp -v .ci/test-cert.pem /etc/letsencrypt/live/nozo.tv/fullchain.pem;
cp -v .ci/test-key.pem /etc/letsencrypt/live/nozo.tv/privkey.pem;
cp -v .ci/dhparam.pem /etc/nginx/